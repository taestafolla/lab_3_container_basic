FROM nginx:1.25.4-alpine
RUN addgroup -S appuser && adduser -S -G appuser appuser
COPY index.html /usr/share/nginx/html
HEALTHCHECK --interval=30s --timeout=3s CMD curl -f http://localhost/ || exit 1
USER appuser
